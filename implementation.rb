class Person
  include Webhook
end

module Webhook
  included do
    before_save   :store_changes
    after_save    :execute_webhook_after_save
    after_destroy :execute_webhook_after_destroy
  end
  
  def store_changes
    @was_new = self.new_record?
    @changes = self.changes.dup
  end
  
  def execute_webhook_after_save
    WebhookBackend.fire(self, (@was_new ? "created" : "updated"), @changes)
  end
  
  def execute_webhook_after_destroy
    WebhookBackend.fire(self, "destroyed", {})
  end
end

module WebHookBackend
  def fire(model, event, changes)
   json = {
     model: model.class.name,
     model_id: model.id,
     event: event,
     changes: changes
   }
   # In some (ordered) background queue
   HTTP::Post.new(find_current_account.webhook_url, body: JSON.dump(json))
  end
end