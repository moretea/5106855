/* Payload for new model */
{
  model: "Company",
  event: "created",
  model_id: 42,
  changes: {
    name: "AwesomeCompany"
  }
}

/* Payload for updated model */
{
  model: "Person",
  event: "update",
  model_id: 42,
  changes: {
    "first_name"=>["Maartn", "Maarten"]}
  }
}

/* Payload for destroyed model */
{
  model: "Person",
  event: "destroyed",
  model_id: 42
}